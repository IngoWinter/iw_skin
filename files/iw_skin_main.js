jQuery(function ($) {

	// normalize login form structure
	var $loginformwrapper = $('#rex-page-login').find('.rex-form-wrapper'),
		$loginbutton = $loginformwrapper.find('.rex-form-submit').detach();
	$loginformwrapper.append($loginbutton);
	$loginbutton.wrap('<div class="rex-form-row"></div>');

	// behavior for rex messages
	$('.rex-message, .rex-message-block').each(function () {
		var $msg = $(this),
			$close = $('<span class="rex-i-close"></span>');
		if ($msg.find('.rex-warning-block, .rex-warning').length) {
			$close.addClass('warning');
		}
		else {
			$close.addClass('info');
		}
		$close.on('click', function () {
			$msg.hide();
		});
		$msg.append($close);
	});

	// colors for addons by installed/ active status
	$('#rex-page-addon').find('tr.rex-addon, tr.rex-plugin').each(function () {
		var $tr = $(this);
		if ($tr.find('.rex-col-c').text().indexOf('ja') !== -1) {
			$tr.addClass('installed');
		}
		if ($tr.find('.rex-col-d').text().indexOf('ja') !== -1) {
			$tr.addClass('active');
		}
	});

});