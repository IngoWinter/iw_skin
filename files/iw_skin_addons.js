jQuery(function ($) {

	// datepicker for xform
	$('.formdate').each(function () {
		var date = new Date(),
			$this = $(this),
			$input_year = $('<input type="hidden" value="" name="" />'),
			$input_month = $('<input type="hidden" value="" name="" />'),
			$input_day = $('<input type="hidden" value="" name="" />'),
			$datepicker = $('<input type="text" value="" readonly />'),
			is_empty = false,
			$reset_datepicker = $('<a class="iw_skin_delete_date">löschen</a>');

		$this.find('select').each(function () {
			var type = this.id.substr(this.id.lastIndexOf('_') + 1)
			$select = $(this);
			if (type === 'year') {
				$input_year.attr('name', $select.attr('name'));
				date.setFullYear($select.val());
			} else if (type === 'month') {
				$input_month.attr('name', $select.attr('name'));
				date.setMonth($select.val() - 1);
			} else {
				$input_day.attr('name', $select.attr('name'));
				date.setDate($select.val());
			}
			if ($select.val() == '00') {
				is_empty = true;
			}
			$select.remove();
		});

		if (is_empty) {
			$input_year.val('00');
			$input_month.val('00');
			$input_day.val('00');
		}
		else {
			$input_year.val(date.getFullYear());
			$input_month.val(date.getMonth() + 1);
			$input_day.val(date.getDate());
		}
		$this.append($input_year, $input_month, $input_day);

		$reset_datepicker.bind('click', function () {
			$datepicker.attr('value', '');
			$.datepicker._clearDate($datepicker);
			$input_year.val('00');
			$input_month.val('00');
			$input_day.val('00');
		});

		$datepicker.datepicker({
			'dateFormat': 'dd.mm.yy',
			'changeMonth': true,
			'changeYear': true,
			'onSelect': function (datestring, datepickerobject) {
				date.setFullYear(datepickerobject.selectedYear);
				date.setMonth(datepickerobject.selectedMonth);
				date.setDate(datepickerobject.selectedDay);
				$input_year.val(date.getFullYear());
				$input_month.val(date.getMonth() + 1);
				$input_day.val(date.getDate());
			},
			prevText: '&#x3c;zurück', prevStatus: '',
			prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
			nextText: 'Vor&#x3e;', nextStatus: '',
			nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
			currentText: 'heute', currentStatus: '',
			todayText: 'heute', todayStatus: '',
			clearText: '-', clearStatus: '',
			closeText: 'schließen', closeStatus: '',
			monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
			monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
			dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
			dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
			dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa']
		});

		if (!is_empty) {
			$datepicker.datepicker('setDate', date);
		}
		$this.append($datepicker, $reset_datepicker);

	});

});